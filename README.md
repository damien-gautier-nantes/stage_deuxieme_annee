# Migration ROS1 -> ROS2
***Author : Damien GAUTIER***  
***Date : 08/18/2023***  

This repo stores contents for the `migration from ROS1 to ROS2`. The structure is the following :  

***Section : <> Code***  
```cpp
-> doc   
        -> images -- images used by the wiki  
```

***Section : Wiki***  
```cpp  
-> A main page with a migration guide on how to move from ROS1 to ROS2.  
-> Other pages which are cited in the main page and which each deal with a different notion of ROS. The idea is to follow the main page at the beginning, but once you've read it you can refer to the other pages independently. 
```